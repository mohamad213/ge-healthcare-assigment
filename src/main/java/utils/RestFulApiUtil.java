package utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import okhttp3.Response;
import org.example.dtos.EntriesApiResponse;
import org.example.dtos.Entry;

import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
public class RestFulApiUtil {

    static final String ApiUrl = "https://api.publicapis.org/entries";
    static ObjectMapper mapper = new ObjectMapper();


    @SneakyThrows
    public static List<Entry> callEntriesAPiAndGetListOfEntries(){
        Response response = HttpClientUtil.sendGetRequest(ApiUrl);

        EntriesApiResponse responseBody;
        try {
            responseBody = mapper.readValue(response.body().string(), EntriesApiResponse.class);

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(String.format("Failed while parsing entries json, error %s", e.getMessage()));
        }
        return responseBody.getEntries();
    }

    public static List<Entry> getEntriesArrayCounter(List<Entry> entriesArrayList){
        List<Entry> authenticationAndAuthorizationList = entriesArrayList.stream().filter(entry->entry.getCategory().equals("Authentication & Authorization")).collect(Collectors.toList());
        printEntries(authenticationAndAuthorizationList);
        return authenticationAndAuthorizationList;
    }

    private static void printEntries(List<Entry> authenticationAndAuthorizationList) {
        authenticationAndAuthorizationList.stream().forEach(Entry::printEntry);
    }

}
