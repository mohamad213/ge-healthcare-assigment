package org.example;

import lombok.SneakyThrows;
import org.example.dtos.Entry;
import org.testng.Assert;
import org.testng.annotations.Test;
import utils.RestFulApiUtil;

import java.util.List;


public class RestFulApiTest {
    static RestFulApiUtil restFulApi = new RestFulApiUtil();
    static List<Entry> entriesArrayList = restFulApi.callEntriesAPiAndGetListOfEntries();

    @SneakyThrows
    @Test
    public static void testEntriesCount() {

        Assert.assertEquals(restFulApi.getEntriesArrayCounter(entriesArrayList).size(), 7, "number of objects where the " +
                "property Category: Authentication & Authorization appears equals to 7");
    }

    @Test
    public static void testEntriesCategory(){

        List<Entry> authenticationAndAuthorizationList = restFulApi.getEntriesArrayCounter(entriesArrayList);
        Assert.assertEquals(authenticationAndAuthorizationList.stream().allMatch(entry -> entry.getCategory().
                equals("Authentication & Authorization")), Boolean.TRUE, "list of entries that we get include " +
                "category that not equal to Authentication & Authorization");
    }
}